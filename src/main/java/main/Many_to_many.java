package main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import ejercicio_one_to_many.Est;
import many_to_many.Curso;
import many_to_many.Estudiante;

public class Many_to_many {
	
	public static void main(String[] args){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence");
		EntityManager em = emf.createEntityManager();
		
		Estudiante es = em.find(Estudiante.class, 1);
		System.out.println(es);
	}

}
